package com.fyntem.lab6springcontroller.controller;

import com.fyntem.lab6springcontroller.entity.DemoUser;
import com.fyntem.lab6springcontroller.model.DemoResponse;
import com.fyntem.lab6springcontroller.service.DemoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController {

    private static final Logger LOG = LoggerFactory.getLogger(DemoController.class);

    @Autowired // Not recommended way of beans injection
    private DemoService demoService;

    @GetMapping("/api/v1/demo/user/{id}")
    public DemoResponse getDemoUserById(@PathVariable final long id) {
        LOG.info("Retrieving demo by id: " + id);
        return toResponse(demoService.getDemoUserById(id));
    }

    // TODO: implement add user, edit username and deleting user from the application
    // (Use for that POST, PUT and DELETE HTTP Queries)

    private static DemoResponse toResponse(final DemoUser demoUser) {
        final DemoResponse response = new DemoResponse();
        response.setId(demoUser.getId());
        response.setName(demoUser.getName());
        return response;
    }
}
