package com.fyntem.lab6springcontroller.model;

public class DemoResponse {

    private long id;
    private String name;

    public DemoResponse() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
