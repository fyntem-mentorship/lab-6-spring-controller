package com.fyntem.lab6springcontroller.repository;

import com.fyntem.lab6springcontroller.entity.DemoUser;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository
public class DemoRepository {

    public static Map<Long, String> demoUsersDB = new HashMap<>();

    static {
        demoUsersDB.put(1L, "Jake");
        demoUsersDB.put(2L, "Josh");
        demoUsersDB.put(3L, "John");
        demoUsersDB.put(4L, "Jane");
        demoUsersDB.put(5L, "Jade");
    }

    public DemoUser getDemoUserById(final long id) {
        if (demoUsersDB.containsKey(id)) {
            return new DemoUser(id, demoUsersDB.get(id));
        } else {
            throw new IllegalArgumentException("Unknown user by id: " + id);
        }
    }
}
