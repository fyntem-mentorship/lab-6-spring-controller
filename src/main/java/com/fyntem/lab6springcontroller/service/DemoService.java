package com.fyntem.lab6springcontroller.service;

import com.fyntem.lab6springcontroller.entity.DemoUser;
import com.fyntem.lab6springcontroller.repository.DemoRepository;
import org.springframework.stereotype.Service;

@Service
public class DemoService {

    private final DemoRepository demoRepository;

    // More recommended way of injecting beans
    public DemoService(DemoRepository demoRepository) {
        this.demoRepository = demoRepository;
    }

    public DemoUser getDemoUserById(final long id) {
        return demoRepository.getDemoUserById(id);
    }
}
