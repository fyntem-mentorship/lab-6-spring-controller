package com.fyntem.lab6springcontroller.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class DemoControllerTest {

    @Autowired // For tests, it is okay to inject beans via annotation
    private DemoController demoController;

    @Test
    public void when_get_demo_user_then_receive_correct_response() {
        // TODO: Implement tests
    }
}
